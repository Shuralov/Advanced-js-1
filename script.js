var Hamburger = function (size, stuffing) {
        this.SIZE = size;
        this.STUFFING = stuffing;

    if (arguments[0] !== 'SIZE_SMALL' && 'SIZE_LARGE') {
        throw "HamburgerException: no size given";
    }

    if (arguments[0] === 'STUFFING_CHEESE' && 'STUFFING_SALAD' && 'STUFFING_POTATO') {
        throw "HamburgerException: first should go size";
    }

    if (arguments[1] !== 'STUFFING_CHEESE' && 'STUFFING_SALAD' && 'STUFFING_POTATO') {
        throw "HamburgerException: no stuffing";
    }

    argsCopy = [];
    for (var b = 0; b < arguments.length; b++) {
        argsCopy[b] = arguments[b];
    }

    for (var a=0; a<argsCopy.length; a++) {
        if (argsCopy[a]===argsCopy[a+1]) {

            throw `HamburgerException: duplicated topping ${argsCopy[a]}`
        }
    }
}

Hamburger.SIZE_SMALL = 'SIZE_SMALL';
Hamburger.SIZE_LARGE = 'SIZE_LARGE';
Hamburger.STUFFING_CHEESE = 'STUFFING_CHEESE';
Hamburger.STUFFING_SALAD = 'STUFFING_SALAD';
Hamburger.STUFFING_POTATO = 'STUFFING_POTATO';
Hamburger.TOPPING_MAYO = 'TOPPING_MAYO';
Hamburger.TOPPING_SPICE = 'TOPPING_SPICE';

var price = {
        SIZE_SMALL : 50,
        SIZE_LARGE : 100,
        STUFFING_CHEESE: 10,
        STUFFING_SALAD: 20,
        STUFFING_POTATO: 15,
        TOPPING_MAYO: 20,
        TOPPING_SPICE: 15
}

var cal = {
    SIZE_SMALL : 20,
    SIZE_LARGE : 40,
    STUFFING_CHEESE: 20,
    STUFFING_SALAD: 5,
    STUFFING_POTATO: 10,
    TOPPING_MAYO: 5,
    TOPPING_SPICE: 0
}


/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 *
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */

var topArr = [];
Hamburger.prototype.addTopping = function (topping) {
    if (arguments.length === 0) {
        throw "HamburgerException: Please, add toppings"
    }
       else
    {
        topArr.push(topping);
        this.TOPPING = topArr;
    }
}

/**
 * Убрать добавку, при условии, что она ранее была
 * добавлена.
 *
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (topping) {
    if (topArr.length>0) {
        topArr.pop();
    }
}

/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function () {
    return topArr;
}

/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function () {
    return this.SIZE;
}

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function () {
    return this.STUFFING;
}

/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function () {
    var priceSet = Object.keys(price);
    var objSet = Object.values(this).flat();
    var accum = 0;

    for (var i=0; i<priceSet.length; i++ ) {
        for (var j=0; j<objSet.length; j++ ) {
            if (priceSet[i] === objSet[j]) {
                accum = accum + price[priceSet[i]];
            }
        }
    }
    return accum;
}

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function () {

    var itemSet = Object.keys(cal);
    var objSet = Object.values(this).flat();
    var accum = 0;

    for (var i=0; i<itemSet.length; i++ ) {
        for (var j=0; j<objSet.length; j++ ) {
            if (itemSet[i] === objSet[j]) {
                accum = accum + cal[itemSet[i]];
            }
        }
    }
    return accum;
}

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */

// маленький гамбургер с начинкой из сыра
var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
// А сколько теперь стоит?
console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер?
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
// Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length); // 1




